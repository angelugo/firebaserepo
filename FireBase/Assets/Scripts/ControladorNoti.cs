using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.Notifications.Android;
public class ControladorNoti : MonoBehaviour
{

    public void ActivarNotificacion()
    {
        DateTime FechaAtivacion = DateTime.Now.AddSeconds(5f);

        CrearNotificacion(FechaAtivacion);
    }
    private const string IdCanal = "IdNoti";
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Permisos()); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CrearNotificacion(DateTime date)
    {
        AndroidNotificationChannel channel = new AndroidNotificationChannel
        {
            Id = IdCanal,
            Name = "CanalNotificacion",
            Description = "Canal para notificaciones",
            Importance = Importance.Default,
        };

        AndroidNotificationCenter.RegisterNotificationChannel(channel);

        AndroidNotification Norificacion = new AndroidNotification
        {
            Title = "Vuelve a Jugar !",
            Text = "Notifiacion de volver a jugar",
            SmallIcon = "Default",
            LargeIcon = "Default",
            FireTime = date,
        };

        AndroidNotificationCenter.SendNotification(Norificacion, IdCanal);
    }

   IEnumerator Permisos() 
    {
        var reques = new PermissionRequest();
        while (reques.Status == PermissionStatus.RequestPending) {

            yield return null;  
        }
    }
}
